package com.codingraja.test;

import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class DeleteHqlQuery {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure();
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Session session = factory.openSession();
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Order ID: ");
		long orderId = scanner.nextLong();
		
		String hql = "delete from Order ord WHERE ord.id=?";
		Query query = session.createQuery(hql);
		query.setParameter(0, orderId);
		
		Transaction transaction = session.beginTransaction();
		query.executeUpdate();
		transaction.commit();
		session.close();
		
		System.out.println("Recored Has been deleted!");
	}

}
