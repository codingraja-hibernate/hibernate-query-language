package com.codingraja.test;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Customer;
import com.codingraja.domain.Order;

public class SaveCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure();
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		List<Order> orders = new ArrayList<Order>();
		orders.add(new Order("Apple MacBook Pro", "MacBook Pro", "Apple", 75000.0));
		orders.add(new Order("Lenovo Thinkpad", "G80", "Lenovo", 125000.0));
		orders.add(new Order("OPPO Camera Phone", "F3", "OPPO", 20000.0));
		
		Customer customer = new Customer("Sandeep", "Kumar", "info@codingraja.com", 9742900123L, orders);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(customer);
		transaction.commit();
		session.close();
		
		System.out.println("Record has been Saved Successfully!");
	}

}
