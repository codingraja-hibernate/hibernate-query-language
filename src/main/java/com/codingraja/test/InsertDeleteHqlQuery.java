package com.codingraja.test;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class InsertDeleteHqlQuery {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Configuration configuration = new Configuration();
		configuration.configure();
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Session session = factory.openSession();
		
		String hql = "INSERT INTO OrderView(itemName,price)"
					+" SELECT ord.itemName, ord.price FROM Order ord";
		Query query = session.createQuery(hql);
		
		Transaction transaction = session.beginTransaction();
		query.executeUpdate();
		transaction.commit();
		session.close();
		
		System.out.println("Dump is created");
	}

}
